/////////////////////////////////////////////////////////////
//                                                         //
//                         BTS Bot                         //
//                                                         //
//             File: getDiscordBearerToken.js              //
//                                                         //
//               Author: Thomas (439bananas)               //
//                                                         //
// Copyright 439bananas 2024 under the Apache 2.0 license. //
//                                                         //
/////////////////////////////////////////////////////////////

import fetch from 'node-fetch'
import getid from './getApplicationId.js'

function getDiscordToken(token, clientsecret, redirecturi, code) {
    return new Promise(function promise(resolve, reject) {
        let id = getid(token)
        fetch("https://discord.com/api/v10/oauth2/token", { // Get the token
            method: 'POST',
            body: new URLSearchParams({
                client_id: id,
                client_secret: clientsecret, // I was an idiot and forgot the underscore then wondered for ages why it wasn't working
                grant_type: 'authorization_code',
                code: code,
                redirect_uri: redirecturi
            }).toString(),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).then(response => response.json())
            .then(response => {
                if (response.error && response.error == "invalid_client") { // If any kinds of errors, reject with x error
                    reject("BAD_CLIENT_SECRET")
                } else if (response.error == "invalid_grant") {
                    reject("BAD_CODE")
                }
                else if (response.error) {
                    reject("UNKNOWN_DISCORD_ERROR")
                    log.error(response.error)
                } else if (response.message) {
                    reject("UNKNOWN_DISCORD_ERROR")
                    log.error(response.message)
                } else {
                    resolve({ "bearertoken": response.access_token, "refreshtoken": response.refresh_token })
                }
            }).catch(err => {
                reject("CANNOT_CONNECT_TO_DISCORD")
            })

    })
}

export default getDiscordToken