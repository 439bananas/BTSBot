/////////////////////////////////////////////////////////////
//                                                         //
//                         BTS Bot                         //
//                                                         //
//                   File: botInGuild.js                   //
//                                                         //
//               Author: Thomas (439bananas)               //
//                                                         //
// Copyright 439bananas 2024 under the Apache 2.0 license. //
//                                                         //
/////////////////////////////////////////////////////////////

import { Router, json } from 'express';
import botInGuild from '../../core/checkBotInGuild';
const router = Router();
const jsonParser = json()

router.post('/*', jsonParser, async (req, res, next) => { // See if the bot is in a guild
    try {
        let url = req.url.split('/')
        if (url[1]) { // If guild ID, check singular guild ID
            let bypassCache = false
            if (req.body.bypassCache) {
                bypassCache = true
            }
            res.status(200).json({ botInGuild: await botInGuild(url[1], bypassCache) })
        } else {
            if (Array.isArray(req.body.guilds)) { // If body is array...
                let guildPresences = {}
                async function getGuildPresences() {
                    for (let guild of req.body.guilds) { // For each item, check if bot is in the guild and add to object
                        guildPresences[guild] = await botInGuild(guild)
                    }

                    if (Object.keys(guildPresences).length == req.body.guilds.length) { // Only upon completion, return object
                        return guildPresences
                    }
                }

                res.status(200).json({ botInGuild: await getGuildPresences() }) // Send object to client
            } else {
                res.status(200).json({ botInGuild: await botInGuild(req.body.guilds) })
            }
        }
    } catch (err) {
        log.temp(err)
        res.status(200).json({ botInGuild: false }) // Send object to client
    }
})

export default router;